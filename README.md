## Install project dependencies

```
npm i @angular/cli -g
```

```
cd fuzzy-web-frontend
npm i
```

## Run project

|        | Development             | Production                 |
|--------|-------------------------|----------------------------|
| Local  | `ng serve -o`           | `ng build -aot`            |
| Remote | `ng serve -e=remote -o` | `ng build -e=staging -aot` |

 `-e` stands for run in specific environment, default `-e=dev`
 
 `-o` just opens the project, can be emitted

## Creating page/component/service

| Generate                                                                                                     | Command                              |
|--------------------------------------------------------------------------------------------------------------|--------------------------------------|
| Page ([Example](https://github.com/fuzzylabs/fuzzy-web-frontend/tree/dev/src/app/private/sales))             | `ng g component private/new-name`    |
| Component ([Example](https://github.com/fuzzylabs/fuzzy-web-frontend/tree/dev/src/app/components/sales-tab)) | `ng g component components/new-name` |
| Service ([Example](https://github.com/fuzzylabs/fuzzy-web-frontend/tree/dev/src/app/services/form))          | `ng g service services/new-name`     |

## Update project

```
npm un @angular/cli -g
npm cache clean
npm i @angular/cli -g
```

```
cd fuzzy-web-frontend
rm -rf node_modules
rd /s/q node_modules
npm i
```
