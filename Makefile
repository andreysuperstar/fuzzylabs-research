GCLOUD_PROJECT:=$(shell gcloud config list project --format="value(core.project)")

.PHONY: all
all: deploy

.PHONY: build
build:
	docker build -t eu.gcr.io/$(GCLOUD_PROJECT)/fuzzy-web-frontend .

.PHONY: push
push: build
	gcloud docker -- push eu.gcr.io/$(GCLOUD_PROJECT)/fuzzy-web-frontend

.PHONY: deploy
deploy: push
	kubectl create -f frontend.yaml

.PHONY: update
update:
	kubectl set image deployment/fuzzy-web-frontend fuzzy-web-static=eu.gcr.io/$(GCLOUD_PROJECT)/fuzzy-web-frontend

.PHONY: delete
delete:
	kubectl delete rc fuzzy-web
	kubectl delete service fuzzy-web
