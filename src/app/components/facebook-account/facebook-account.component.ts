import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { Router } from '@angular/router';
import { FacebookService, InitParams, LoginResponse, LoginOptions, LoginStatus } from 'ngx-facebook';

import { ConfirmComponent } from '../confirm/confirm.component';
import { AccountsService } from '../../services/accounts/accounts.service';

import { environment } from '../../../environments/environment';


const ASSETS_BASE_URL = environment.assetsBaseUrl;


@Component({
  selector: 'facebook-account',
  templateUrl: './facebook-account.component.html',
  styleUrls: ['./facebook-account.component.scss']
})
export class FacebookAccountComponent implements OnInit {

  providerImagePath: string;
  confirmData: object;
  isConfirmed: boolean;

  @Input() connectionInfo: any;
  @ViewChild('facebookAccountModal') facebookAccountModal: ModalDirective;
  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;

  constructor(private accountsService: AccountsService, private router: Router,
    private fb: FacebookService) {
    this.providerImagePath = `${ASSETS_BASE_URL}/images/facebook.png`;

    this.confirmData = {
      title: 'Warning: Suspend segment publishing',
      content: `Disconnecting the account will stop any segments from being published to it.`
    };
    this.isConfirmed = false;

    const initParams: InitParams = {
      appId: '406974749684235',
      xfbml: true,
      version: 'v2.9'
    };

    this.fb.init(initParams)
      .then((response: any) => {
        console.log('facebook.init + ', response);
      })
      .catch((error: any) => {
        console.log('facebook.init - ', error);
      });
  }

  ngOnInit() {
    this.getLoginStatus()
      .then((response: any) => {
        console.log('promise getloginstatus', response);

        if (this.connectionInfo.isConnected) {
          this.getFacebookUserInfo();
        }
      })
      .catch((error: any) => {
        console.log('promise getloginstatus', error);

        this.connectionInfo.isConnected = false;
      });
  }

  showModal() {
    this.facebookAccountModal.show();
  }

  onConnect() {
    if (!this.connectionInfo.isConnected) {

      this.login()
        .then((response: any) => {
          console.log('facebook.login', response);

          if (this.connectionInfo.isConnected) {
            this.getFacebookUserInfo();
          }
        })
        .catch((error: any) => {
          console.log('facebook.login', error);

          this.connectionInfo.isConnected = false;
        });
    }
  }

  onTest() {
    this.getFacebookAdaccountInfo()
      .then((response: any) => {
        console.log('test successful');
      })
      .catch((error: any) => {
        console.log('test failed');
      });
  }

  onDisconnect(): void {
    if (!this.isConfirmed) {
      this.confirm.onShow();
      return;
    }

    this.logout()
      .then((response: any) => {
        console.log('facebook.logout', response);

        this.deleteConnection();
      })
      .catch((error: any) => {
        console.log('facebook.logout', error);
      });
  }

  onConfirm(event): void {
    if (event) {
      this.isConfirmed = event;
    }

    this.onDisconnect();

    this.isConfirmed = false;
  }

  getLoginStatus(): Promise<any> {
    return this.fb.getLoginStatus()
      .then((response: LoginStatus) => {
        console.log('facebook.getLoginStatus + ', response);

        if (response.status === 'connected') {
          this.connectionInfo.isConnected = true;
          this.connectionInfo.accountId = response.authResponse.userID;
          this.connectionInfo.accessToken = response.authResponse.accessToken;
        } else {
          this.connectionInfo.isConnected = false;
        }

        return response;
      });
  }

  login(): Promise<any> {
    const loginOptions: LoginOptions = {
        scope: 'public_profile,email,ads_management,ads_read,read_insights,manage_pages,business_management,pages_show_list',
        return_scopes: true,
        enable_profile_selector: true
      };

    return this.fb.login(loginOptions)
      .then((response: LoginResponse) => {
        console.log('facebook.login + ', response);

        this.connectionInfo.isConnected = true;
        this.connectionInfo.accountId = response.authResponse.userID;
        this.connectionInfo.accessToken = response.authResponse.accessToken;

        return response;
      });
  }

  logout(): Promise<any> {
    return this.fb.logout()
      .then((response: LoginResponse) => {
        console.log('facebook.login', response);

        this.connectionInfo.isConnected = false;

        return response;
      });
  }

  getFacebookUserInfo(): Promise<any> {
    return this.fb.api('/me?fields=id,name,email', 'get')
      .then((response: any) => {
        console.log('facebook.getEmail', response);

        this.connectionInfo.name = response['name'];
        this.connectionInfo.email =  response['email'];

        if (!this.connectionInfo.email) {
          console.log('facebook.getEmail', 'Add email to your fb acc.');
        } else {
          console.log('User doesn\'t have email');

          this.addConnection(this.connectionInfo.email, this.connectionInfo.accountId,
            this.connectionInfo.accessToken);
        }

        return response;
      })
      .catch((error: any) => {
        console.log('facebook.getEmail', error);

        return error;
      });
  }

  getFacebookAdaccountInfo(): Promise<any> {
    return this.fb.api('/me/adaccounts', 'get')
      .then((response: any) => {
        console.log('facebook.adacc', response);

        return response;
      })
      .catch((error: any) => {
        console.log('facebook.addacc', error);
      });
  }

  addConnection(email: string, accountId: string, accessToken: string): void {
    this.accountsService
      .onConnectToFacebook(email, accountId, accessToken)
      .subscribe(response => {
        console.log('fb component connect', response);

        // tslint:disable-next-line:forin
        for (const connection in response) {
          this.connectionInfo.id = response[connection].id;
        }

        console.log('after connect', this.connectionInfo.id);
      });
  }

  deleteConnection(): void {
    console.log('before disconnect', this.connectionInfo);

    this.accountsService
      .onDisconnectFromFacebook(this.connectionInfo.id)
      .subscribe((response: any) => {
        console.log('fb component connect', response);

        this.connectionInfo = { 'isConnected': false };
      });
  }
}
