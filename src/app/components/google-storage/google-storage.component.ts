import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ConfirmComponent } from '../confirm/confirm.component';


@Component({
  selector: 'google-storage',
  templateUrl: './google-storage.component.html',
  styleUrls: ['./google-storage.component.scss']
})
export class GoogleStorageComponent implements OnInit {

  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;
  form: FormGroup;
  authorize: FormGroup;
  submitted: boolean;
  uploaded: boolean;
  confirmData: object;
  confirmed: boolean;
  validationMessages: Object;
  validationClasses: any;
  formErrors: object[];

  constructor(private formBuilder: FormBuilder) {
    this.submitted = false;
    this.uploaded = true;
    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      }
    };
    this.validationMessages = {
      'url': {
        'required': {
          'message': 'URL is required.'
        },
        'pattern': {
          'type': 'warning',
          'message': 'URL format must be <em>gs://bucket/name.ext</em>'
        }
      }
    };
    this.confirmData = {
      title: 'Warning: Import deletes current sales data',
      content: `Reimporting sales data will delete all existing sales data that was imported previously.`
    };
    this.confirmed = false;
    this.formErrors = [];
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      'type': 'gs',
      'url': [
        null,
        [
          Validators.required,
          Validators.pattern(/^(gs:\/\/)([\da-z\-]+)\/([\/\w \-]*)*\.([a-z\.]{2,6})$/)
        ]
      ],
      'isAuthorized': null,
      'schedule': this.formBuilder.group({
        'reimport': null,
        'type': {
          value: null,
          disabled: true
        }
      })
    });
    this.authorize = this.formBuilder.group({
      'accountId': null,
      'accessToken': null,
      'refreshToken': null
    });

    this.form
      .valueChanges
      .subscribe(form => this.onValueChanged(form));
    this.form
      .get('schedule.reimport')
      .valueChanges
      .subscribe(reimport => this.onReimportValueChanged(reimport));

    this.onValueChanged();
  }

  onBlur() {
    this.onValueChanged();
  }

  onValueChanged(data?: FormGroup): void {
    if (!this.form) {
      return;
    }

    const form = this.form;
    this.formErrors = [];

    for (const field in this.validationMessages) {
      if (this.validationMessages.hasOwnProperty(field)) {
        const control = form.get(field);

        if (control && (control.touched || control.touched && control.dirty) && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {

            if (control.errors.hasOwnProperty(key)) {
              // this.formErrors[field] += messages[key];
              const error = messages[key];
              this.formErrors.push(error);
            }
          }
        }
      }
    }

    const urlTouched = form.get('url').touched,
          urlValid = form.get('url').valid,
          urlSuccess = urlTouched && urlValid,
          urlDanger = urlTouched && !urlValid;

    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': urlSuccess,
          'has-danger': urlDanger
        },
        'formControl': {
          'form-control-success': urlSuccess,
          'form-control-danger': urlDanger
        }
      }
    };
  }

  onReimportValueChanged(reimport: boolean): void {
    if (reimport) {
      this.form
        .get('schedule.type')
        .enable();
    } else {
      this.form
        .get('schedule.type')
        .disable();
    }
  }

  onSubmit(form: FormGroup): void {
    if (this.uploaded && !this.confirmed) {
      this.confirm.onShow();
      return;
    }
    this.submitted = true;
    console.log(this.form.value);
  }

  onConfirm(event: boolean): void {
    if (event) {
      this.confirmed = event;
    }
    this.onSubmit(this.form);
  }

}
