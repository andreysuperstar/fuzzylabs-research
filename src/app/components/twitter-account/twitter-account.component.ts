import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { ConfirmComponent } from '../confirm/confirm.component';

import { environment } from '../../../environments/environment';


const ASSETS_BASE_URL = environment.assetsBaseUrl;


@Component({
  selector: 'twitter-account',
  templateUrl: './twitter-account.component.html',
  styleUrls: ['./twitter-account.component.scss']
})
export class TwitterAccountComponent implements OnInit {

  providerImagePath: string;
  confirmData: object;
  isConfirmed: boolean;

  @Input() connectionInfo: any;
  @ViewChild('twitterAccountModal') twitterAccountModal: ModalDirective;
  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;

  constructor() {
    this.providerImagePath = `${ASSETS_BASE_URL}/images/twitter.jpg`;

    this.confirmData = {
      title: 'Warning: Suspend segment publishing',
      content: `Disconnecting the account will stop any segments from being published to it.`
    };
    this.isConfirmed = false;
  }

  ngOnInit() {
  }

  showModal() {
    this.twitterAccountModal.show();
  }

  onConnect(): void {
    this.connectionInfo.isConnected = true;
  }

  onDisconnect(): void {
    if (!this.isConfirmed) {
      this.confirm.onShow();
      return;
    }

    this.connectionInfo.isConnected = false;
  }

  onConfirm(event): void {
    if (event) {
      this.isConfirmed = event;
    }

    this.onDisconnect();

    this.isConfirmed = false;
  }

}
