import { Component, OnInit, OnChanges, Input } from '@angular/core';

import { Alert } from '../../models/alert.model';


@Component({
  selector: 'alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit, OnChanges {

  @Input('alerts') alertsData: object[];
  alerts: Alert[];

  constructor() {
    this.alertsData = [];
  }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.mapAlerts(this.alertsData);
  }

  mapAlerts(alertsData: object[]): Alert[] {
    this.alerts = [];

    for (let i = 0; i < alertsData.length; i++) {
      const alertData = alertsData[i];
      this.alerts.push(this.createAlert(alertData));
    }

    return this.alerts;
  }

  createAlert(alertData: object): Alert {
    return new Alert(alertData);
  }

}
