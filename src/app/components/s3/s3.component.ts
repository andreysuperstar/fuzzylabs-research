import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ConfirmComponent } from '../confirm/confirm.component';


@Component({
  selector: 's3',
  templateUrl: './s3.component.html',
  styleUrls: ['./s3.component.scss']
})
export class S3Component implements OnInit {

  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;
  form: FormGroup;
  keyLength: number;
  secretLength: number;
  submitted: boolean;
  uploaded: boolean;
  confirmData: object;
  confirmed: boolean;
  validationMessages: Object;
  validationClasses: any;
  formErrors: object[];

  constructor(private formBuilder: FormBuilder) {
    this.keyLength = 20;
    this.secretLength = 40;
    this.submitted = false;
    this.uploaded = true;
    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      },
      'key': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      },
      'secret': {
        'formGroup': {
          'has-success': false,
          'has-danger': false
        },
        'formControl': {
          'form-control-success': false,
          'form-control-danger': false
        }
      }
    };
    this.validationMessages = {
      'url': {
        'required': {
          'message': 'URL is required.'
        },
        'pattern': {
          'type': 'warning',
          'message': 'URL format must be <em>s3://bucket/name.ext</em>'
        }
      },
      'key': {
        'required': {
          'message': 'Key is required.'
        },
        'minlength': {
          'type': 'warning',
          'message': `Key must be ${this.keyLength} characters long.`
        },
        'maxlength': {
          'type': 'warning',
          'message': `Key must be ${this.keyLength} characters long.`
        }
      },
      'secret': {
        'required': {
          'message': 'Secret is required.'
        },
        'minlength': {
          'type': 'warning',
          'message': `Secret must be ${this.secretLength} characters long.`
        },
        'maxlength': {
          'type': 'warning',
          'message': `Secret must be ${this.secretLength} characters long.`
        }
      }
    };
    this.confirmData = {
      'title': 'Warning: Import deletes current sales data',
      'content': `Reimporting sales data will delete all existing sales data that was imported previously.`
    };
    this.confirmed = false;
    this.formErrors = [];
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      'type': 's3',
      'url': [
        null,
        [
          Validators.required,
          Validators.pattern(/^(s3:\/\/)([\da-z\-]+)\/([\/\w \-]*)*\.([a-z\.]{2,6})$/)
          // (gs:\/\/)[a-z0-9_\-\.]+\/.+
          // Validators.pattern(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/)
        ]
      ],
      'key': [
        null,
        [
          Validators.required,
          Validators.minLength(this.keyLength),
          Validators.maxLength(this.keyLength)
        ]
      ],
      'secret': [
        null,
        [
          Validators.required,
          Validators.minLength(this.secretLength),
          Validators.maxLength(this.secretLength)
        ]
      ],
      'schedule': this.formBuilder.group({
        'reimport': null,
        'type': {
          value: null,
          disabled: true
        }
      })
    });

    this.form
      .valueChanges
      .subscribe(form => this.onValueChanged(form));
    this.form
      .get('schedule.reimport')
      .valueChanges
      .subscribe(reimport => this.onReimportValueChanged(reimport));

    this.onValueChanged();
  }

  onBlur() {
    this.onValueChanged();
  }

  onValueChanged(data?: FormGroup): void {
    if (!this.form) {
      return;
    }

    const form = this.form;
    this.formErrors = [];

    for (const field in this.validationMessages) {
      if (this.validationMessages.hasOwnProperty(field)) {
        const control = form.get(field);
        // console.log(`Control Errors ${field}`, control.errors);

        if (control && (control.touched || control.touched && control.dirty) && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {

            if (control.errors.hasOwnProperty(key)) {
              // this.formErrors[field] += messages[key];
              const error = messages[key];
              this.formErrors.push(error);
            }
          }
        }
      }
    }

    const urlTouched = form.get('url').touched,
          urlValid = form.get('url').valid,
          urlSuccess = urlTouched && urlValid,
          urlDanger = urlTouched && !urlValid,
          keyTouched = form.get('key').touched,
          keyValid = form.get('key').valid,
          keySuccess = keyTouched && keyValid,
          keyDanger = keyTouched && !keyValid,
          secretTouched = form.get('secret').touched,
          secretValid = form.get('secret').valid,
          secretSuccess = secretTouched && secretValid,
          secretDanger = secretTouched && !secretValid;

    this.validationClasses = {
      'url': {
        'formGroup': {
          'has-success': urlSuccess,
          'has-danger': urlDanger
        },
        'formControl': {
          'form-control-success': urlSuccess,
          'form-control-danger': urlDanger
        }
      },
      'key': {
        'formGroup': {
          'has-success': keySuccess,
          'has-danger': keyDanger
        },
        'formControl': {
          'form-control-success': keySuccess,
          'form-control-danger': keyDanger
        }
      },
      'secret': {
        'formGroup': {
          'has-success': secretSuccess,
          'has-danger': secretDanger
        },
        'formControl': {
          'form-control-success': secretSuccess,
          'form-control-danger': secretDanger
        }
      }
    };
  }

  onReimportValueChanged(reimport: boolean): void {
    if (reimport) {
      this.form
        .get('schedule.type')
        .enable();
    } else {
      this.form
        .get('schedule.type')
        .disable();
    }
  }

  onSubmit(form: FormGroup): void {
    if (this.uploaded && !this.confirmed) {
      this.confirm.onShow();
      return;
    }
    this.submitted = true;
    console.log(form.get('schedule.reimport').value);
  }

  onConfirm(event: boolean): void {
    if (event) {
      this.confirmed = event;
    }
    this.onSubmit(this.form);
  }

}
