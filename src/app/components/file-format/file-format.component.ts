import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'file-format',
  templateUrl: './file-format.component.html',
  styleUrls: ['./file-format.component.scss']
})
export class FileFormatComponent implements OnInit {

  fields: string[];

  constructor() {
    this.fields = [
      'Order_ID', 'Order_Item_ID',
      'Datetime',
      'Order_Item_Name', 'Order_Item_Price', 'Order_Item_Quantity',
      'Customer_Name', 'Customer_Email',
      'Order_Item_Cost', 'Order_Item_Discount',
      'Customer_Mobile', 'Customer_Gender',
      'Order_Item_Category', 'Order_Item_Brand', 'Order_Item_Currency',
      'Delivery_Address_Country', 'Delivery_Address_City', 'Delivery_Address_PO',
      'Website_URL'
    ];
  }

  ngOnInit() {
  }

}
