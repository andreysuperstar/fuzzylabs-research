import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  @Input() data: any;
  @ViewChild('confirm') confirm: ModalDirective;
  @Output() prompt: EventEmitter<boolean> = new EventEmitter();

  constructor() {
    this.data = {
      title: 'Modal Title',
      content: 'One fine body…'
    };
  }

  ngOnInit() {
  }

  onShow(): void {
    this.confirm.show();
  }

  onHide(): void {
    this.confirm.hide();
  }

  onCancel(): boolean {
    this.prompt.emit(false);
    this.onHide();
    return false;
  }

  onOK(): boolean {
    this.prompt.emit(true);
    this.onHide();
    return true;
  }

}
