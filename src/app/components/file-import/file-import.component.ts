import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FileUploader, FileUploaderOptions, FileItem } from 'ng2-file-upload/ng2-file-upload';
import { ConfirmComponent } from '../confirm/confirm.component';

import { CookieService } from '../../services/cookie/cookie.service';
import { FileImportService } from '../../services/file-import/file-import.service';


const URL = 'http://35.187.122.146/api/imports/v1/file/';


@Component({
  selector: 'file-import',
  templateUrl: './file-import.component.html',
  styleUrls: ['./file-import.component.scss']
})
export class FileImportComponent implements OnInit {

  @ViewChild(ConfirmComponent) readonly confirm: ConfirmComponent;
  form: FormGroup;
  submitted: boolean;
  uploaded: boolean;
  confirmData: object;
  confirmed: boolean;
  formErrors: object[];

  accessToken: any;
  uploader: FileUploader;
  uploaderOptions: FileUploaderOptions;
  hasBaseDropZoneOver: boolean;
  hasAnotherDropZoneOver: boolean;

  constructor(private formBuilder: FormBuilder, private fileImportService: FileImportService) {

    this.accessToken = CookieService.get('access_token');
    this.uploaderOptions = {
      'headers': [
        {
          'name': 'Content-Disposition',
          'value': 'attachment; filename=upload_test.json'
        }
      ],
      'url': `${URL}?access_token=${this.accessToken}`,
      'disableMultipart': true
    };
    this.uploader = new FileUploader(this.uploaderOptions);
    this.uploader.onBeforeUploadItem = (fileItem: FileItem) => {
      fileItem.withCredentials = false;
      return fileItem;
    };
    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;

    this.submitted = false;
    this.uploaded = true;
    this.confirmData = {
      title: 'Warning: Import deletes current sales data',
      content: `Reimporting sales data will delete all existing sales data that was imported previously.`
    };
    this.confirmed = false;
    this.formErrors = [
      {
        message: `<strong>CSV is invalid format.</strong> Please check the following errors:
        <br>
        &ndash; error 1…`
      }
    ];
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.form = this.formBuilder.group({
      'file': null
    });

    this.form
      .valueChanges
      .subscribe(form => this.onValueChanged(form));

    this.onValueChanged();
  }

  onChange(event: any): void {
    const input = event.target;
    // console.log(input);
    const isFile = input.getAttributeNode('ngControl') && input.getAttribute('ngControl') === 'file';
    const hasFile = input.files && input.files[0];
    if (isFile && hasFile) {
      const fileReader = new FileReader();
      fileReader
        .onloadstart = (ev: Event): void => {
          // console.log('onloadstart', ev);
          const fileName = input.files[0].name;
          input.setAttribute('data-content', fileName);
        };
      fileReader
        .onload = (ev: Event): any => {
          // console.log('onload', ev);
          const file = ev.target['result'];
          // console.log('File', file);
        };
      fileReader.readAsDataURL(input.files[0]);
    }
  }

  onValueChanged(data?: FormGroup): void {
    if (!this.form) {
      return;
    }
  }

  onSubmit(form: FormGroup): void {
    if (this.uploaded && !this.confirmed) {
      this.confirm.onShow();
      return;
    }
    this.submitted = true;
  }

  onConfirm(event: boolean): void {
    if (event) {
      this.confirmed = event;
    }
    this.onSubmit(this.form);
  }

  fileOverBase(event: any): void {
    this.hasBaseDropZoneOver = event;
  }

  fileOverAnother(event: any): void {
    this.hasAnotherDropZoneOver = event;
  }

}
