import { Component, OnInit, OnChanges, Input} from '@angular/core';

@Component({
  selector: 'provider-connection',
  templateUrl: './provider-connection.component.html',
  styleUrls: ['./provider-connection.component.scss']
})
export class ProviderConnectionComponent implements OnInit, OnChanges {

  @Input() isConnected: boolean;
  classes: string[];

  constructor() { }

  ngOnInit() {
    this.isConnected = true;
  }

  ngOnChanges() {
    this.classes = [];
    this.classes.push(
      ...(
        this.isConnected
        ? ['fa', 'fa-check-circle', 'connection-success']
        : ['fa', 'fa-times-circle', 'connection-danger']
      )
    );
  }

}
