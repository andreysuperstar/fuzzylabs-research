import { Component, OnInit, ViewChild } from '@angular/core';

import { FacebookAccountComponent } from '../../components/facebook-account/facebook-account.component';
import { AdwordsAccountComponent } from '../../components/adwords-account/adwords-account.component';
import { TwitterAccountComponent } from '../../components/twitter-account/twitter-account.component';

import { AccountsService } from '../../services/accounts/accounts.service';

import { environment } from '../../../environments/environment';


const ASSETS_BASE_URL = environment.assetsBaseUrl;


@Component({
  selector: 'accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  isConnected: boolean = false;
  facebookConnection: any = { };
  adwordsConnection: any = { };
  twitterConnection: any = { };
  facebookImagePath: string;
  adwordsImagePath: string;
  twitterImagePath: string;

  @ViewChild(FacebookAccountComponent) readonly facebookAccountModal: FacebookAccountComponent;
  @ViewChild(AdwordsAccountComponent) readonly adwordsAccountModal: AdwordsAccountComponent;
  @ViewChild(TwitterAccountComponent) readonly twitterAccountModal: TwitterAccountComponent;

  constructor(private accountsService: AccountsService) {
    this.facebookImagePath = `${ASSETS_BASE_URL}/images/facebook.png`;
    this.adwordsImagePath = `${ASSETS_BASE_URL}/images/adwords`;
    this.twitterImagePath = `${ASSETS_BASE_URL}/images/twitter.jpg`;

    this.adwordsConnection.isConnected = true;
    this.twitterConnection.isConnected = true;
  }

  ngOnInit() {
    // this.getUserConnections();
  }

  showFacebookAccountModal() {
    this.facebookAccountModal.showModal();
  }

  showAdwordsAccountModal() {
    this.adwordsAccountModal.showModal();
  }

  showTwitterAccountModal() {
    this.twitterAccountModal.showModal();
  }

  getUserConnections() {
    this.accountsService
      .onGetUserConnections()
      .subscribe(response => {
        console.log('this.accountsService.onGetUserConnections().subscribe()', response);

        // tslint:disable-next-line:forin
        for (const connection in response) {
          if (response[connection]['provider'] === 'FB') {
            this.facebookConnection = response[connection];
            this.facebookConnection.isConnected = true;
          }

          if (response[connection]['provider'] === 'ADWORDS') {
            this.adwordsConnection = response[connection];
            this.adwordsConnection.isConnected = true;
          }

          if (response[connection]['provider'] === 'TWITTER') {
            this.twitterConnection = response[connection];
            this.twitterConnection.isConnected = true;
          }
        }
      });
  }

}
