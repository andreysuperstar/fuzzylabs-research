import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  tabs: object[];
  alerts: object[];

  constructor() {
    this.tabs = [
      { heading: 'File Import' },
      { heading: 'S3 Bucket' },
      { heading: 'Google Storage Bucket' }
    ];
    this.alerts = [
      {
        type: 'warning',
        message: '<strong>Warning!</strong> No sales data is imported yet',
      },
      {
        type: 'success',
        message: '<strong>Well done!</strong> your sales data is imported via File import',
        bucket: 'Google Storage bucket'
      }
    ];
  }

  ngOnInit() {
  }

}
