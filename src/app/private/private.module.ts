import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';

import { FacebookModule } from 'ngx-facebook';

import { PrivateRoutingModule } from './private-routing.module';

import { StarterComponent } from './starter/starter.component';
import { SalesComponent } from './sales/sales.component';
import { FileImportComponent } from '../components/file-import/file-import.component';
import { S3Component } from '../components/s3/s3.component';
import { GoogleStorageComponent } from '../components/google-storage/google-storage.component';
import { FileFormatComponent } from '../components/file-format/file-format.component';
import { FacebookAccountComponent } from '../components/facebook-account/facebook-account.component';
import { AdwordsAccountComponent } from '../components/adwords-account/adwords-account.component';
import { TwitterAccountComponent } from '../components/twitter-account/twitter-account.component';
import { AccountsComponent } from './accounts/accounts.component';
import { ProviderConnectionComponent } from '../components/provider-connection/provider-connection.component';

import { FileImportService } from '../services/file-import/file-import.service';
import { FormService } from '../services/form/form.service';
import { AccountsService } from '../services/accounts/accounts.service';


@NgModule({
  imports: [
    PrivateRoutingModule,
    SharedModule,
    ChartsModule,
    ReactiveFormsModule,
    TabsModule,
    FacebookModule,
    FileUploadModule
  ],
  declarations: [
    StarterComponent,
    SalesComponent,
    FileImportComponent,
    S3Component,
    GoogleStorageComponent,
    FileFormatComponent,
    FacebookAccountComponent,
    AdwordsAccountComponent,
    TwitterAccountComponent,
    AccountsComponent,
    ProviderConnectionComponent
  ],
  providers: [
    FileImportService,
    FormService,
    AccountsService
  ]
})
export class PrivateModule { }
