import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FormService } from '../../services/form/form.service';


@Component({
  selector: 'starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.scss']
})
export class StarterComponent implements OnInit  {

  form: FormGroup;
  results: any;

  constructor(private formBuilder: FormBuilder, private formService: FormService) {
    this.form = this.formBuilder.group({
      'message': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(500)
        ])
      ],
    });
  }

  ngOnInit() {
  }

  onSubmit(form) {
    console.log(form.get('message').value);
    const message = form.get('message').value;
    this.formService
      .onSubmit(message)
      .subscribe(response => {
        console.log('this.formService.onSubmit().subscribe()', response);
        this.formService.id = response['id'];
        console.log('this.formService.id', this.formService.id);
        this.getData();
      });
  }

  getData() {
    this.formService
      .onResults(this.formService.id)
      .subscribe(response => {
        console.log('this.getData().onResults().subscribe()', response);
        this.results = 'results';
      });
  }

}
