import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalesComponent } from './sales/sales.component';
import { StarterComponent } from './starter/starter.component';
import { AccountsComponent } from './accounts/accounts.component';


const routes: Routes = [
  {
    path: '',
    // data: {
    //   title: 'Private'
    // },
    children: [
      {
        path: '',
        redirectTo: 'sales',
        pathMatch: 'full'
      },
      {
        path: 'sales',
        component: SalesComponent,
        data: {
          title: 'Sales'
        }
      },
      {
        path: 'starter',
        component: StarterComponent,
        data: {
          title: 'Starter'
        }
      },
      {
        path: 'accounts',
        component: AccountsComponent,
        data: {
          title: 'Accounts'
        }
      },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule {}
