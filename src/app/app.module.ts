import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';

import { FacebookModule } from 'ngx-facebook';

// Routing Module
import { AppRoutingModule } from './app.routing';

// Layouts
import { BlankLayoutComponent } from './layouts/blank-layout/blank-layout.component';
import { FullLayoutComponent } from './layouts/full-layout/full-layout.component';

import { NotFoundErrorComponent } from './components/404/404.component';
import { InternalServerErrorComponent } from './components/500/500.component';

import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth/auth.service';
import { ApiHttpService } from './services/api-http/api-http.service';


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    FacebookModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    BlankLayoutComponent,
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    NotFoundErrorComponent,
    InternalServerErrorComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    },
    {
      provide: APP_BASE_HREF,
      useValue: '/app'
    },
    AuthGuard,
    AuthService,
    ApiHttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
