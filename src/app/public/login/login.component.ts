import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment.staging';

import { AuthService } from '../../services/auth/auth.service';


const PREFILL = environment.prefill;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {};

  ngOnInit() { }

  onLogin(username: string, password: string) {
    this.authService.isLoggedIn.subscribe();
    this.authService
      .login(username, password)
      .subscribe(val =>
        this.router.navigate([this.authService.redirectUrl]).then());
  }

  onSignUp(username: string, password: string) {
    console.log(username, password);
  }
}
