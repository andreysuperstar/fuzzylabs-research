import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AlertsComponent } from '../components/alerts/alerts.component';
import { ProgressbarComponent } from '../components/progressbar/progressbar.component';
import { ConfirmComponent } from '../components/confirm/confirm.component';


@NgModule({
  imports: [
    CommonModule,
    AlertModule,
    ModalModule
  ],
  declarations: [
    AlertsComponent,
    ProgressbarComponent,
    ConfirmComponent
  ],
  exports: [
    CommonModule,
    AlertModule,
    ModalModule,
    AlertsComponent,
    ProgressbarComponent,
    ConfirmComponent
  ]
})
export class SharedModule { }
