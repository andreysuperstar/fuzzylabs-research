import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { CookieService } from '../cookie/cookie.service';
import { ApiHttpService } from '../api-http/api-http.service';



const LOGIN_URL = 'auth';
const LOGIN_PATH = 'get_token';


@Injectable()
export class AuthService {
  isLoggedIn: BehaviorSubject<boolean> = new  BehaviorSubject(false);
  // store the URL so we can redirect after logging in
  redirectUrl = 'sales';

  constructor(private apiHttpService: ApiHttpService) {
    this.isLoggedIn.subscribe();
  }

  private saveToken(res: Response | any): Observable<boolean> {
    if ('access_token' in res && 'refresh_token' in res && 'expires_in' in res) {
      CookieService.set(res['refresh_token'], 'refresh_token');
      CookieService.set(res['access_token'], 'access_token');
      CookieService.set(true, 'is_token_valid', res['expires_in']);
      this.isLoggedIn.next(true);
    }
    return Observable
      .of(this.isLoggedIn.getValue());
  }

  public checkTokens(can_refresh_tokens: boolean = true): Observable<boolean> {
    const [access_token, refresh_token, is_token_valid] =
      [CookieService.get('access_token'), CookieService.get('refresh_token'), CookieService.get('is_token_valid')];

    if (is_token_valid) {  // if token not expired yet
      return Observable.of(true);
    } else if (can_refresh_tokens) {
      if (refresh_token && access_token) {  // send refresh request if we have all necessary tokens
        const params = {
          'refresh_token': refresh_token
        };
        return this.apiHttpService.execute('Post', 'auth', 'refresh_token', params, true)
          .switchMap(res => this.saveToken(res));
      }
    }
    return Observable.of(false);
  }

  login(email: string, password: string, url: string = LOGIN_URL, path: string = LOGIN_PATH): Observable<boolean> {
    const params = {
      'username': email,
      'password': password
    };
    return this.apiHttpService
      .execute('Post', url, path, params, false)
      .switchMap(res => this.saveToken(res));
  }

  logout(): void {
    this.isLoggedIn.next(false);
  }

}
