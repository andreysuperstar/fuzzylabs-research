import { Injectable } from '@angular/core';

import * as Cookies from 'js-cookie';

const COOKIE_NAMES = ['access_token', 'is_token_valid', 'refresh_token'];


@Injectable()
export class CookieService {

  static set(value: any, name: string, expires_in?: number): boolean {
    if (expires_in) {
      const expiration_time = new Date(new Date().getTime() + 0.9 * 1000 * expires_in);
      Cookies.set(name, value, { 'expires': expiration_time });
    } else {
      Cookies.set(name, value, { 'expires': 365 });
    }
    return true;
  }

  static clear() {
    for (const cookie_name of COOKIE_NAMES) {
      Cookies.remove(cookie_name);
    }
  }


  static all() {
    return Cookies.get();
  }

  static get(name) {
    return Cookies.get(name);
  }

  static remove(name) {
    return Cookies.remove(name);
  }

  constructor() { }

}
