import {Injectable, Injector} from '@angular/core';
import { Http, RequestOptions, RequestMethod, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment';

import { CookieService } from '../cookie/cookie.service';
import {AuthService} from "../auth/auth.service";
import {Router} from "@angular/router";


const API_URL = environment.apiUrl;
const PUBLIC_API_URL = environment.publicApiUrl;
const API_VERSION = environment.apiVersion;


@Injectable()
export class ApiHttpService {
  private authService: AuthService;

  constructor(private http: Http, private router: Router, private injector: Injector) {
    setTimeout(() => this.authService = this.injector.get(AuthService));  // timeout for circular dependencies
  }

  execute(method: string, url: string, path: string, params?: { [key: string]: any | any[] }, auth: boolean = true): Observable<Response> {
    let api_url = PUBLIC_API_URL;
    const requestConfig = {
      method: RequestMethod[method],
      body: params,
    };

    if (auth) {
      api_url = API_URL;
      const access_token = CookieService.get('access_token');
      requestConfig['params'] = { 'access_token': access_token };
    }
    const options = new RequestOptions(requestConfig);
    const fullPath = `${api_url}/${url}/${API_VERSION}/${path}`;
    console.log(fullPath);
    return this.http
      .request(fullPath, options)
      .map(this.extractData)
      .catch(error => this.handleError(error));
  }

  private extractData(res: Response) {
    console.log('extractData()', res);
    const body = res.json();
    console.log('body', body);
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    if (errMsg.toLowerCase().includes('token')) {
      this.authService.redirectUrl = this.router.url;
      this.router.navigate(['/login']).then();
      CookieService.clear()
    }
    return Observable.throw(errMsg);
  }
}
