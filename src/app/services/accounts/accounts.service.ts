import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { ApiHttpService } from '../api-http/api-http.service';


const CONNECTIONS_URL = 'connections';
const USER_CONNECTIONS_PATH = 'user';
const FACEBOOK_CONNECTION_PATH = 'fb';


@Injectable()
export class AccountsService {

  constructor(private apiHttpService: ApiHttpService) { }

  onGetUserConnections(path: string = USER_CONNECTIONS_PATH): Observable<Response> {
    return this.apiHttpService.execute('Get', CONNECTIONS_URL, path);
  }

  onConnectToFacebook(email: string, accountId: string, accessToken: string,
    path: string = FACEBOOK_CONNECTION_PATH): Observable<Response> {
    const params = {
      'email': email,
      'account_id': accountId,
      'token': accessToken
    };

    return this.apiHttpService.execute('Post', CONNECTIONS_URL, path, params);
  }

  onDisconnectFromFacebook(id: string): Observable<Response> {
    const path: string = FACEBOOK_CONNECTION_PATH + '/' + id;

    return this.apiHttpService.execute('Delete', CONNECTIONS_URL, path);
  }

}
