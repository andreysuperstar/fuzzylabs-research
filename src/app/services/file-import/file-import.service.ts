import { Injectable } from '@angular/core';

import { ApiHttpService } from '../api-http/api-http.service';
import { CookieService } from '../cookie/cookie.service';


@Injectable()
export class FileImportService {

  accessToken: string;
  refreshToken: string;

  constructor(private apiHttpService: ApiHttpService) { }

  some() {
    this.accessToken = CookieService.get('access_token');
    this.refreshToken = CookieService.get('refresh_token');
  }

}
