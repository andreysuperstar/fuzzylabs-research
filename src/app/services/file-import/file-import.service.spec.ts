import { TestBed, inject } from '@angular/core/testing';

import { FileImportService } from './file-import.service';

describe('FileImportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileImportService]
    });
  });

  it('should be created', inject([FileImportService], (service: FileImportService) => {
    expect(service).toBeTruthy();
  }));
});
