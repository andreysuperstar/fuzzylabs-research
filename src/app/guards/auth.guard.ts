import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, CanActivateChild, CanLoad, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';

import { AuthService } from '../services/auth/auth.service';


const PREFILL = environment.prefill;


@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const url: string = state.url;

    return this.checkLogin(url);
  }

  canLoad(route: Route): Observable<boolean> | boolean {
    const url: string = route.path;
    return this.checkLogin(url);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(next, state);
  }

  checkLogin(url: string): Observable<boolean> {
    if (PREFILL) {
      return Observable.of(true);
    }
    else {
      return this.authService.checkTokens(true).map(val => {
        if (!val) {
          console.log('User need to re-enter credentials');
          // Store the attempted URL for redirecting
          this.authService.redirectUrl = url;
          this.router.navigate(['/login']).then();
          return false;
        }
        return true;
      });
    }
  }
}
