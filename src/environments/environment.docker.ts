export const environment = {
  production: false,
  prefill: false,
  publicApiUrl: 'http://127.0.0.1:8000',
  apiUrl: 'http://127.0.0.1:9000/api',
  apiVersion: 'v1',
  assetsBaseUrl: '/assets'
};
