export const environment = {
  production: false,
  prefill: true,
  apiUrl: 'http://35.187.122.146/api',
  publicApiUrl: 'http://35.187.122.146',
  apiVersion: 'v1',
  assetsBaseUrl: '/assets'
};
