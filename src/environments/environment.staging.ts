export const environment = {
  production: true,
  prefill: false,
  apiUrl: 'http://35.187.122.146/api',
  publicApiUrl: 'http://35.187.122.146',
  apiVersion: 'v1',
  assetsBaseUrl: '/assets'
};
